const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const { userSignupValidator } = require("../helpers/validator");
const { signup, signin, logout, requireSignin } = require("../controllers/auth");

router.post(
  "/signup",
  [
    check("name", "Name field is required").not().isEmpty(),
    check("email", "Email is not valid").isEmail(),
    check("password")
      .isLength({ min: 6 })
      .withMessage("must be at least 5 chars long")
      .matches(/\d/)
      .withMessage("must contain a number"),
  ],
  userSignupValidator,
  signup
);

router.post("/signin", signin);

router.get("/logout", logout);


module.exports = router;
