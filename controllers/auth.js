const User = require("../models/user");
const jwt = require("jsonwebtoken");
const expressJwt = require("express-jwt");

const { errorHandler } = require("../helpers/dbErrorHandler");

/**
 * user signup
 */
exports.signup = (req, res) => {
  //   console.log("OUTPUT:", req.body);
  const user = new User(req.body);
  user.save((err, user) => {
    if (err) {
      return res.status(400).json({
        err: errorHandler(err),
      });
    }
    user.salt = undefined;
    user.hashed_password = undefined;
    res.json({
      user,
    });
  });
};

/**
 * user login
 */
exports.signin = (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email }, (err, user) => {
    if (err || !user) {
      return res.status(400).json({
        err: "User with eamil does not exists!",
      });
    }
    if (!user.authenticate(password)) {
      return res.status(401).json({
        error: "Email and Password do not match!",
      });
    }
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
    res.cookie("t", token, { expire: new Date() + 9999 });
    const { _id, name, email, role } = user;
    return res.json({ token, user: { _id, email, name, role } });
  });
};

/**
 * 
 * @param {'*'} req 
 * @param {*} res 
 * User logout
 */

exports.logout = (req, res) => {
  res.clearCookie("t");
  res.json({
    message: "Succefully logout",
  });
};

/**
 * user authenticating via token and for access user needs to signin
 */
exports.requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  userProperty: "auth",
  algorithms: ["HS256"],
});
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * only allowed if user is authenticated
 */
exports.isAuth = (req, res, next) => {
  let user = req.profile && req.auth && req.profile._id == req.auth._id;
  if (!user) {
    return res.status(403).json({
      error: "Access Denied",
    });
  }
  next();
};
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * only allowed if user is admin
 */
exports.isAdmin = (req, res, next) => {
  if (req.profile.role === 0) {
    return res.status(403).json({
      error: "Admin only, Access Denied",
    });
  }
  next();
};


