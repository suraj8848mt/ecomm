const Category = require("../models/category");
const { errorHandler } = require("../helpers/dbErrorHandler");
const category = require("../models/category");

/**
 *
 * @param {*} req
 * @param {*} res
 * CREATE NEW CATEGORY
 */
exports.create = (req, res) => {
  const category = new Category(req.body);
  category.save((err, data) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler,
      });
    }
    res.json({
      status: "success",
      data,
    });
  });
};

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @param {*} id
 * GET Category by ID
 */
exports.categoryById = (req, res, next, id) => {
  Category.findById(id).exec((err, category) => {
    if (err || !category) {
      return res.status(400).json({
        error: errorHandler(err),
      });
    }
    req.category = category;
    next();
  });
};

/**
 *
 * @param {*} req
 * @param {*} res
 * GET Category
 */
exports.read = (req, res) => {
  return res.json(req.category);
};

/**
 *
 * @param {*} req
 * @param {*} res
 * Category Update
 */
exports.update = (req, res) => {
  const category = req.category;
  category.name = req.body.name;
  category.save((err, data) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler(err),
      });
    }
    res.json(data);
  });
};

/**
 *
 * @param {*} req
 * @param {*} res
 * Category Delete
 */
exports.remove = (req, res) => {
  const category = req.category;
  category.remove((err, data) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler(err),
      });
    }
    res.json({
      success: true,
      message: "Category Deleted",
    });
  });
};

/**
 *
 * @param {*} req
 * @param {*} res
 * List all Category
 */
exports.list = (req, res) => {
  Category.find().exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler(err),
      });
    }
    res.json({
      sucess: true,
      data: data,
    });
  });
};
